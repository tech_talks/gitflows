# Possible release workflows

## Agenda
- The problem
- Freeze with deployment from master
- GitFlow workflow
- OneFlow 
- GitHub flow
- GitLab flow
- Simple Git workflow
- Summary


## The problem
- Before deployment one needs to:****
    - update changelog
    - run tests on external testing framework
- Above procedures should not influence other development procedures


## 1. Freeze with deployment from master
![alt text](freezing/Freezing flow.svg "short_branch_view")
### 1.1. Verbal freeze
pros:

cons:
- freezing 
    - DEVS: 
      - for some time unable to merge to master
    - RELEASER: 
        - need of communicating freeze and unfreeze
        - sometimes DEVS miss information and make releasing harder by merging to master
    
### 1.2. Freezing with locking branch
pros:

cons:
- freezing 
    - DEVS: 
      - for some time unable to merge to master (distracts DEVS)
    - RELEASER: 
      - need of communicating freeze and unfreeze
      - need of locking and unlocking branch


## 2. GitFlow workflow
There are two main branches:
- Branch name for production releases - most common name [master] 
- Branch name for "next release" development - most common name [develop]

![alt text](GitFlow/short_branch.svg "short_branch_view")

Except them there are standard feature branches. But also release branch.
![alt text](GitFlow/release_branch.svg "short_branch_view")

references:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow


## 3. OneFlow 
Difference to 1. is that all deployment is happening on the release branch, with tagging.

![alt text](OneFlow/release-branch-merge-final.png "short_branch_view")

references:
- https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow#release-branches


## 4. GitHub flow
Flow created for canary development (very often released, with small changes).
Only production branch and feature branches. 
Deployment is from feature branch so in case of problems in production it can be 
immediately reverted to master.

![alt text](GitHub/GithubFlow.png "short_branch_view")

![alt text](GitHub/gitlab_flow_github_flow.png "short_branch_view")

references:
- https://guides.github.com/introduction/flow/
- https://medium.com/swlh/git-flow-vs-github-flow-3ad44bd46407


## 5. GitLab flow
### 5.1. Production branch
![alt text](GitLab/gitlab_flow_production_branch.png "short_branch_view")

### 5.2. Environment branch
In our case because of needed changes to master, pre-prod would merge to master as well.

![alt text](GitLab/gitlab_flow_environment_branches.png "short_branch_view")

### 5.3. Release Branch
![alt text](GitLab/gitlab_flow_release_branches.png "short_branch_view")

references:
- https://docs.gitlab.com/ee/topics/gitlab_flow.html
- https://stackoverflow.com/questions/39917843/what-is-the-difference-between-github-flow-and-gitlab-flow


## 6. Simple Git workflow
The simple workflow has two guiding principles:
- master is always production-like and deployable.
- rebase during feature development, explicit (non fast-forward) merge when done.

references:
- https://www.atlassian.com/blog/git/simple-git-workflow-is-simple


## Summary
- Freezing is inconvenient and should be avoided.
- Simple Git is usable only for very small projects 
- OneFlow and GitHub flow are similar, but in GitHub flow there is release per branch
- In OneFlow all changes are done on the release branch (no freeze), releases are made from that branch, not from master
- GitLab Flow gives many ways of solving the problem
    - Production branch - do not give space for changelog and version changes(without making freeze)
    - Environment branch - eases automation
    - Release branch - Similar to freezing, we do not have to keep those branches but the release is from the branch
        - in contrary to OneFlow it's point is for supporting many release verions while OneFlows' point is to make release independent


TODO:
- zdefiniowane czym jest release czym jest deploy
- informacja o ze cos sie dzieje
- update gitflow image
- update github flow
- dodac porownanie
